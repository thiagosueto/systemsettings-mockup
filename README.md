# systemsettings-mockup

This is just a mockup application used to design mockups for System Settings.

This shouldn't be used as a reference, copy-paste code, or treated as a standalone application.

To run on Plasma 5:

```cmake
cmake -B build/
cmake --build build/
./build/systemsettings-mockup
```

To run on Plasma 6:

```cmake
# First, source the prefix.sh of your self-built Kirigami Addons
cmake -B build/ -DBUILD_WITH_QT6
cmake --build build/
./build/bin/systemsettings-mockup
```

My proposed, opinionated ordering currently looks like so
and is just a draft idea without much thought for now:

![](Mockup1.png)

![](Mockup2.png)
