import QtQuick 2.15
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard
import org.kde.kirigamiaddons.settings 1.0 as KirigamiSettings

Kirigami.ApplicationWindow {
    id: root
    visibility: Window.Maximized

    Component.onCompleted: {
        // Expected warning: Unable to assign CategorizedSettings_QMLTYPE to Page_QMLTYPE
        pageStack.push(systemsettings)
    }

    Component {
        id: systemsettings
        KirigamiSettings.CategorizedSettings {
            actions: [

                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "WORKSPACE   ------------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "General Behavior"
                    icon.name: "settings-configure"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Language"
                    icon.name: "applications-education-language"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Accessibility"
                    icon.name: "preferences-desktop-accessibility"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Search"
                    icon.name: "preferences-desktop-search"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },



                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "DEVICES   ------------------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Display"
                    icon.name: "video-display"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Touchscreen"
                    icon.name: "preferences-desktop-touchscreen"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Mouse and Touchpad"
                    icon.name: "preferences-desktop-cursors"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Keyboard"
                    icon.name: "input-keyboard"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Drawing Tablet"
                    icon.name: "input-tablet"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Game Controller"
                    icon.name: "input-gamepad"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },


                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "HARDWARE   -------------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Sound"
                    icon.name: "preferences-desktop-sound"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Power"
                    icon.name: "preferences-system-power-management"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Bluetooth"
                    icon.name: "preferences-system-bluetooth"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Disks and Cameras"
                    icon.name: "preferences-system-disks"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Thunderbolt"
                    icon.name: "preferences-desktop-thunderbolt"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },



                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "NETWORK   ---------------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Wi-fi"
                    icon.name: "applications-internet"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Firewall"
                    icon.name: "firewall-config"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Proxy"
                    icon.name: "preferences-system-network-proxy"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "File Sharing"
                    icon.name: "preferences-system-network-share-windows"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Online Accounts"
                    icon.name: "preferences-online-accounts"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },






                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "APPLICATIONS   ---------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Default Applications"
                    icon.name: "preferences-desktop-default-applications"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Application Permissions"
                    icon.name: "applications-other"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Notifications"
                    icon.name: "preferences-desktop-notification"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Updates"
                    icon.name: "system-software-update"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },




                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "CUSTOMIZATION   -----------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Theme"
                    icon.name: "preferences-desktop-theme-global"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Animations"
                    icon.name: "preferences-desktop-effects"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Fonts"
                    icon.name: "preferences-desktop-font"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Window Management"
                    icon.name: "preferences-system-window-behavior"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Activities"
                    icon.name: "preferences-desktop-activities"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },


                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "SECURITY & PRIVACY   ------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Users"
                    icon.name: "preferences-desktop-user"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Feedback"
                    icon.name: "preferences-desktop-feedback"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Lockscreen"
                    icon.name: "emblem-encrypted-locked"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },



                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "SYSTEM   -------------------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "About System"
                    icon.name: "help-about"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Session"
                    icon.name: "preferences-system-session-services"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },
                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "Autostart"
                    icon.name: "applications-all"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                },



                KirigamiSettings.SettingAction {
                    actionName: "general"
                    text: "UNCATEGORIZED   ------------------"
                    page: Qt.resolvedUrl("SettingsPage.qml")
                }

            ]
        }
    }
}
