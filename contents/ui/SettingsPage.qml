import QtQuick 2.15
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.formcard 1.0 as FormCard

FormCard.FormCardPage {
    id: root

    title: "Settings Page"

    FormCard.FormHeader {
        title: "A Generic Settings Page"
    }

    FormCard.FormCard {
        FormCard.FormTextDelegate {
            text: "Just a sample settings page for mockups"
        }
    }
}
