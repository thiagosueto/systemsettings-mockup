#include <QtQml>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>

int main(int argCount, char* argVector[])
{
    QApplication app(argCount, argVector);

    KAboutData aboutData(
        QStringLiteral("systemsettings-mockup"),
        i18n("System Settings Mockup"));

    KAboutData::setApplicationData(aboutData);

    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("kde")));

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.load(QStringLiteral("qrc:/mockup/main.qml"));
    app.exec();
}

